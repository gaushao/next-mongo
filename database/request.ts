import axios, { Method } from 'axios'
import { API_URL } from './constants'
import { HTTPMethod } from './types'

interface Data<T> {
  data: T
}

export async function get<T>(path: string) {
  const { data }: Data<T> = await axios(`${API_URL}${path}`)
  return data
}

export async function request<T>(
  path: string,
  data: T,
  method: Method = HTTPMethod.POST as HTTPMethod
) {
  const res = await axios({
    method,
    url: `${API_URL}${path}`,
    data,
  })
  return data
}
