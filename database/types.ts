import { Method } from 'axios'
import { UpdateOneOptions } from 'mongodb'

export enum HTTPMethod {
  GET = 'GET',
  DELETE = 'DELETE',
  HEAD = 'HEAD',
  OPTIONS = 'OPTIONS',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  PURGE = 'PURGE',
  LINK = 'LINK',
  UNLINK = 'UNLINK',
}

export enum DatabaseDocumentProperty {
  ID = 'id',
  _ID = '_id',
}

export type DatabaseDocumentProperties = {
  [DatabaseDocumentProperty.ID]: string
  [DatabaseDocumentProperty._ID]: string
}

export type DocumentType = DatabaseDocumentProperties & Record<string, any>

export type CollectionType = { name: string }
export type CollectionResponseType = CollectionType & {
  documents: DocumentType[]
}

export type DocumentQuery = {
  collection?: string
  document?: Record<string, any>
}
export type UpdateDocumentQuery = {
  collection: string
  document?: Partial<DocumentType>
  options?: UpdateOneOptions
}
