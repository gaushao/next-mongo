import { DatabaseDocumentProperty } from './types'

export const PUBLIC_URL = 'http://localhost:3000/'
export const API_PATH = 'api/'
export const API_URL = `${PUBLIC_URL}${API_PATH}`

export const LOCKED_DOCUMENT_PROPERTIES = [
  DatabaseDocumentProperty.ID,
  DatabaseDocumentProperty._ID,
]
