import { UpdateRequest as APIUpdateRequest } from '../pages/api/update'
import { get, request } from './request'
import {
  DocumentType,
  CollectionType,
  CollectionResponseType,
  DocumentQuery,
  UpdateDocumentQuery,
} from './types'

export type CollectionQuery = CollectionType
export type CollectionResponse = CollectionResponseType
export type CollectionRequest = (
  query: CollectionQuery
) => Promise<CollectionResponse>
export const collection: CollectionRequest = async (query) => {
  try {
    const name = query?.name
    const documents = await get<DocumentType[]>(`collection?name=${name}`)
    console.log(`database collection ${name} documents `, documents)
    return { name, documents }
  } catch (e) {
    return e
  }
}

export type CollectionsQuery = undefined
export type CollectionsResponse = CollectionType[]
export type CollectionsRequest = (
  query: CollectionsQuery
) => Promise<CollectionsResponse>
export const collections: CollectionsRequest = async () => {
  try {
    const res = await get<CollectionType[]>('collections')
    console.log('database collections', res)
    return res
  } catch (e) {
    return e
  }
}

export type RemoveQuery = DocumentQuery
export type RemoveResponse = DocumentQuery
export type RemoveRequest = (query: RemoveQuery) => Promise<RemoveResponse>
export const remove: RemoveRequest = async (query) => {
  try {
    const res = await request<DocumentQuery>('remove', query)
    console.log('database remove', res)
    return query
  } catch (e) {
    return e
  }
}

export type UpdateDocumentReqMethod = APIUpdateRequest['method']
export type UpdateQuery = UpdateDocumentQuery & {
  method: UpdateDocumentReqMethod
}
export type UpdateResponse = UpdateQuery
export type UpdateRequest = (query: UpdateQuery) => Promise<UpdateResponse>
export const update = async (query: UpdateQuery) => {
  try {
    const { method } = query
    const res = await request<UpdateDocumentQuery>(
      'update',
      {
        options: { upsert: true },
        ...query,
      },
      method
    )
    console.log('database update', res)
    return query
  } catch (e) {
    return e
  }
}

export type InsertQuery = DocumentQuery
export type InsertResponse = InsertQuery
export type InsertRequest = (query: InsertQuery) => Promise<InsertResponse>
export const insert: InsertRequest = async (query: InsertQuery) => {
  try {
    const { collection, document } = query
    const res = await request<InsertQuery>('insert', { document, collection })
    console.log('database insert', res)
    return res
  } catch (e) {
    return e
  }
}

export const drop = async (query?: DocumentQuery) => {
  try {
    const res = await request<DocumentQuery>('drop', query)
    console.log('database drop', res)
    return res
  } catch (e) {
    return e
  }
}
