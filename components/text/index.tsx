import styled from 'styled-components'

export const P = styled.span`
  font-family: sans-serif;
`

export const H1 = styled.h1`
  font-family: sans-serif;
`
