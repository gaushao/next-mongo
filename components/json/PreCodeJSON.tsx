const PreCodeJSON = ({ children }: { children: Record<string, string> }) => (
  <pre>
    <code>{JSON.stringify(children, null, '\t')}</code>
  </pre>
)

export default PreCodeJSON
