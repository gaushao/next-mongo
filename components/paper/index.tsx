import { FC } from 'react'
import styled from 'styled-components'
import { omit } from 'lodash'
import css from '../../style/css'

type PageStyleProps = {
  flex?: boolean
  hide?: boolean
  backgroundColor?: string
  width?: string
  height?: string
  margin?: string
  padding?: string
  border?: string
  justifyContent?: string
}

const Paper = styled.div(
  ({ flex, hide, ...styles }: PageStyleProps) => `
    ${flex ? 'display: flex;' : ''}
    ${hide ? 'display: none;' : ''}
    ${css<PageStyleProps>(omit(styles, ['flex', 'hide']))}
  `
)

Paper.defaultProps = {
  children: null,
  flex: false,
  backgroundColor: 'transparent',
}

export default Paper as FC<PageStyleProps>
