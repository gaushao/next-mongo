import Paper from '.'

type Props = {
  w?: number
  h?: number
}

const px = (v?: number) => (v ? `${v}px` : undefined)

function Spacer({ w, h }: Props) {
  return <Paper width={px(w)} height={px(h)} />
}

export default Spacer
