import styled from 'styled-components'

const Button = styled.button`
  font-family: sans-serif;
  cursor: pointer;
`

export default Button
