import { useCallback, useState } from 'react'
import { noop } from 'lodash'

type RequestCallback<Req, Res> = (...req: Req[]) => Promise<Res>
type LoadFn<Req> = (query?: Req) => void
type UseAsync<Req, Res> = {
  result: Res
  load: LoadFn<Req>
  loading: boolean
  error: Error
}
export type OnSuccess<Res> = (data: Res) => void
export type OnError = (error: Error) => void

function useAsync<Req, Res>(
  request: RequestCallback<Req, Res>,
  onSuccess: OnSuccess<Res> = noop,
  onError: OnError = noop
): UseAsync<Req, Res> {
  const [result, setResult] = useState<Res>(null)
  const [loading, setLoading] = useState<boolean>(false)
  const [error, setError] = useState<Error>(null)
  const getResponse = useCallback((res: Res) => {
    setLoading(false)
    setResult(res)
    onSuccess(res)
  }, [])
  const catchError = useCallback((e: Error) => {
    setLoading(false)
    setError(e)
    onError(e)
  }, [])
  const load: LoadFn<Req> = useCallback(
    (query) => {
      setError(null)
      setLoading(true)
      request(query).then(getResponse).catch(catchError)
    },
    [getResponse, catchError]
  )
  return {
    result,
    load,
    loading,
    error,
  }
}

export default useAsync
