import { remove } from '../../database/service'
import useAsync, { OnSuccess } from './useAsync'

export type OnSuccessCallback = OnSuccess<unknown>

function useRemove(onSuccess?: OnSuccessCallback) {
  return useAsync<unknown>(remove, onSuccess)
}

export default useRemove
