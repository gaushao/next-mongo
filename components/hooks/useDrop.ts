import { drop } from '../../database/service'
import useAsync, { OnSuccess } from './useAsync'

export type OnSuccessCallback = OnSuccess<unknown>

function useDrop(onSuccess?: OnSuccessCallback) {
  return useAsync<unknown>(drop, onSuccess)
}

export default useDrop
