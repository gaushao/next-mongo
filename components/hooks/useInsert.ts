import { insert } from '../../database/service'
import useAsync, { OnSuccess } from './useAsync'

export type OnSuccessCallback = OnSuccess<unknown>

function useInsert(onSuccess?: OnSuccessCallback) {
  return useAsync<unknown>(insert, onSuccess)
}

export default useInsert
