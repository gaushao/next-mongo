import {
  collection,
  CollectionQuery,
  CollectionResponse,
} from '../../database/service'
import { CollectionResponseType } from '../../database/types'
import useAsync, { OnSuccess } from './useAsync'

export type OnSuccessCallback = OnSuccess<CollectionResponseType>

function useCollection(onSuccess?: OnSuccessCallback) {
  return useAsync<CollectionQuery, CollectionResponse>(collection, onSuccess)
}

export default useCollection
