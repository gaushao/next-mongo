import {
  collections,
  CollectionsQuery,
  CollectionsResponse,
} from '../../database/service'
import useAsync, { OnSuccess } from './useAsync'

export type OnSuccessCallback = OnSuccess<CollectionsResponse>

function useCollections(onSuccess?: OnSuccessCallback) {
  return useAsync<CollectionsQuery, CollectionsResponse>(collections, onSuccess)
}

export default useCollections
