import { update, UpdateQuery, UpdateResponse } from '../../database/service'
import useAsync, { OnSuccess } from './useAsync'

export type OnSuccessCallback = OnSuccess<UpdateResponse>

function useUpdate(onSuccess?: OnSuccessCallback) {
  return useAsync<UpdateQuery, UpdateResponse>(update, onSuccess)
}

export default useUpdate
