import Paper from '../paper'
import Spacer from '../paper/Spacer'
import { P } from '../text'

type Props = {
  children: JSX.Element | JSX.Element[]
  label: string
}

function Field({ children, label }: Props) {
  return children ? (
    <Paper flex>
      <P>{label}</P>
      <Spacer w={10} />
      {children}
    </Paper>
  ) : null
}

export default Field
