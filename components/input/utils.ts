import { ChangeEvent } from 'react';


export function onChangeEventTarget<E>(cb: Function) {
  return (event: ChangeEvent<E>) => cb(event.target);
}

export default {};
