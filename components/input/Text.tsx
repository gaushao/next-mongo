import Field from './Field'
import { onChangeEventTarget } from './utils'
import { useCallback } from 'react'

type OnChangeCallback = {
  onChange: (value: string) => void
}
function onChangeInputEvent(onChange) {
  return onChangeEventTarget<HTMLInputElement>(({ value }) => onChange(value))
}

export type InputProps = {
  label?: string
  value: any
} & OnChangeCallback

function Input({ onChange, value }: InputProps) {
  const onChangeInput = useCallback(onChangeInputEvent(onChange), [onChange])
  return <input type='text' onChange={onChangeInput} value={value} />
}

function TextInput({ onChange, label, value }: InputProps) {
  return label ? (
    <Field label={label}>
      <Input onChange={onChange} value={value} />
    </Field>
  ) : (
    <Input onChange={onChange} value={value} />
  )
}

TextInput.defaultProps = {
  label: undefined,
}

export default TextInput
