import List from './List'
import { Provider } from './context'

export default function Collections() {
  return (
    <Provider>
      <List />
    </Provider>
  )
}
