import Paper from '../paper'
import Input from '../input/Text'
import Button from '../button'
import useSelection from './context/hooks/useSelection'
import useInsert from './context/hooks/useInsert'
import useCollection from './context/hooks/useCollection'

function Create() {
  const { selected, setSelected, init, unselect, isNamed } = useSelection()
  const { insert } = useInsert()
  const { result } = useCollection(selected)
  return selected === null ? (
    <Button onClick={init}>ADD</Button>
  ) : (
    <Paper flex>
      <Input label='collection name' onChange={setSelected} value={selected} />
      {isNamed && !result && <Button onClick={insert}>CREATE</Button>}
      <Button onClick={unselect}>X</Button>
    </Paper>
  )
}

export default Create
