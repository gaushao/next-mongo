import { useCallback, useEffect } from 'react'
import useContext from './useContext'
import useCollection, { OnSuccessCallback } from '../../../hooks/useCollection'

function useContextCollection(collection: string) {
  const { collections, setCollections } = useContext()
  const onSuccess = useCallback<OnSuccessCallback>(
    ({ name, documents }) => {
      setCollections((current) => ({
        ...current,
        [name]: {
          name,
          documents: documents
            .map((document) => ({
              ...document,
              collection: name,
            }))
            .reverse(),
        },
      }))
    },
    [collection]
  )
  const { load, loading, error } = useCollection(onSuccess)
  const reload = useCallback(
    (res?: { collection: string }) => {
      if (!loading) {
        const name = res?.collection || collection
        load({ name })
      }
    },
    [load, collection]
  )
  const result = collections[collection]
  return {
    result,
    reload,
    loading,
    error,
  }
}

export default useContextCollection
