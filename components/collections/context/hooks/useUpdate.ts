import { useCallback } from 'react'
import { Document, DocumentData } from '../types'
import useUpdate from '../../../hooks/useUpdate'
import useCollection from './useCollection'
import { HTTPMethod } from '../../../../database/types'

function useContextUpdate(document: Document) {
  const { collection } = document
  const { reload } = useCollection(collection)
  const { result, load, loading, error } = useUpdate(reload)
  const update = useCallback(
    (changes: DocumentData) => () => {
      load({
        collection,
        document: { ...document, ...changes },
        method: HTTPMethod.PUT,
      })
    },
    [collection, document]
  )
  const unset = useCallback(
    (property: string) => () => {
      load({
        collection,
        document: { id: document.id, [property]: document[property] },
        method: HTTPMethod.DELETE,
      })
    },
    [collection, document]
  )
  return { result, update, unset, loading, error }
}

export default useContextUpdate
