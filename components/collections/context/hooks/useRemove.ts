import { useCallback } from 'react'
import { Document } from '../../context/types'
import useRemove from '../../../hooks/useRemove'
import useCollection from './useCollection'

function useContextRemove(document: Document) {
  const { collection } = document
  const { reload } = useCollection(collection)
  const { result, load, loading, error } = useRemove(reload)
  const remove = useCallback(() => {
    load({ collection, document })
  }, [collection, document])
  return { result, remove, loading, error }
}

export default useContextRemove
