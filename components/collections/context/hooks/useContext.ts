import { useContext } from 'react'
import { Context } from '..'

const useCollectionsContext = () => useContext(Context)

export default useCollectionsContext
