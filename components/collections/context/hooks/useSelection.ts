import { useCallback, useMemo } from 'react'
import useContext from './useContext'
import { isEmpty } from 'lodash'

function useContextSelection() {
  const { selected, setSelected } = useContext()
  const init = useCallback(() => {
    setSelected('')
  }, [])
  const unselect = useCallback(() => {
    setSelected(null)
  }, [])
  const isNamed = useMemo(() => !isEmpty(selected), [selected])
  return { init, unselect, isNamed, selected, setSelected }
}

export default useContextSelection
