import { useCallback } from 'react'
import useDrop from '../../../hooks/useDrop'
import useCollections from './useCollections'

function useContextDrop(collection: string) {
  const { load: reload } = useCollections()
  const { result, load, loading, error } = useDrop(reload)
  const drop = useCallback(() => {
    load({ collection })
  }, [collection])
  return { result, drop, loading, error }
}

export default useContextDrop
