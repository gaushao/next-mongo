import { useCallback } from 'react'
import useInsert from '../../../hooks/useInsert'
import useCollections from './useCollections'
import useSelection from './useSelection'
import useDocument from './useDocument'

function useContextInsert() {
  const { document } = useDocument()
  const { selected } = useSelection()
  const { load: reload } = useCollections()
  const { result, load, loading, error } = useInsert(reload)
  const insert = useCallback(() => {
    if (selected) load({ collection: selected, document })
  }, [selected, document, load])
  return { result, insert, loading, error }
}

export default useContextInsert
