import { useCallback, useMemo } from 'react'
import { get, isEmpty } from 'lodash'
import useContext from './useContext'

function useContextDocument() {
  const { document, setDocument } = useContext()
  const valid = useMemo(
    () => document && !isEmpty(document.key) && !isEmpty(document.value),
    [document]
  )
  const setKey = useCallback((key: string) => {
    setDocument((d) => ({ ...d, key }))
  }, [])
  const setValue = useCallback((value: any) => {
    setDocument((d) => ({ ...d, value }))
  }, [])
  return { document, valid, setKey, setValue }
}

export default useContextDocument
