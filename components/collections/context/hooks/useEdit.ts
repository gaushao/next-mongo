import useContext from './useContext'

function useContextEdit() {
  const { edit, setEdit } = useContext()
  return { edit, setEdit }
}

export default useContextEdit
