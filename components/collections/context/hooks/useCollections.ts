import { useCallback } from 'react'
import useContext from './useContext'
import useCollections, {
  OnSuccessCallback,
} from '../../../hooks/useCollections'

function useContextCollections() {
  const { names, setNames, collections, setCollections } = useContext()
  const onSuccess = useCallback<OnSuccessCallback>((data) => {
    const mappedByName = data.map(({ name }) => name)
    const reducedByName = data.reduce(
      (acc, { name }) =>
        name
          ? {
              ...acc,
              // null documents will trigger its loading when needed
              [name]: { name, documents: null },
            }
          : acc,
      {}
    )
    setNames(mappedByName)
    setCollections(reducedByName)
  }, [])
  const { load, loading, error } = useCollections(onSuccess)
  return { names, collections, load, loading, error }
}

export default useContextCollections
