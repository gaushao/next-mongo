import { Dispatch, SetStateAction } from 'react'
import { CollectionType, DocumentType } from '../../../database/types'

export enum ContextDocumentProperty {
  COLLECTION = 'collection',
}
export type ContextDocumentProperties = {
  [ContextDocumentProperty.COLLECTION]: string
}
export type DocumentData = Record<string, string>
export type Document = DocumentType & ContextDocumentProperties & DocumentData
export type Collection = {
  documents: Document[]
} & CollectionType
export type Collections = Record<string, Collection>

export type ContextType = {
  names: string[]
  setNames: Dispatch<SetStateAction<string[]>>
  edit: string
  setEdit: Dispatch<SetStateAction<string>>
  selected: string
  setSelected: Dispatch<SetStateAction<string>>
  collections: Collections
  setCollections: Dispatch<SetStateAction<Collections>>
  document: DocumentData
  setDocument: Dispatch<SetStateAction<DocumentData>>
}
