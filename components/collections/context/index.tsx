import { useState, createContext, useMemo } from 'react'
import { ContextType, Collections, DocumentData } from './types'
import { EMPTY_CONTEXT } from './constants'

export const Context = createContext<ContextType>(EMPTY_CONTEXT)

export function Provider({ children }) {
  const [edit, setEdit] = useState<string>(EMPTY_CONTEXT.edit)
  const [selected, setSelected] = useState<string>(EMPTY_CONTEXT.selected)
  const [document, setDocument] = useState<DocumentData>(EMPTY_CONTEXT.document)
  const [names, setNames] = useState<string[]>(EMPTY_CONTEXT.names)
  const [collections, setCollections] = useState<Collections>(
    EMPTY_CONTEXT.collections
  )
  const value = useMemo(
    () => ({
      names,
      setNames,
      edit,
      setEdit,
      selected,
      setSelected,
      collections,
      setCollections,
      document,
      setDocument,
    }),
    [
      names,
      setNames,
      edit,
      setEdit,
      selected,
      setSelected,
      collections,
      setCollections,
      document,
      setDocument,
    ]
  )
  return <Context.Provider value={value}>{children}</Context.Provider>
}
