import { noop } from 'lodash'
import { LOCKED_DOCUMENT_PROPERTIES as API_PROPS } from '../../../database/constants'
import { ContextDocumentProperty } from './types'
import { ContextType, Collections, DocumentData } from './types'

export const LOCKED_DOCUMENT_PROPERTIES = [
  ...API_PROPS,
  ContextDocumentProperty.COLLECTION,
]

export const EMPTY_DOCUMENT: DocumentData = {}
export const EMPTY_CONTEXT: ContextType = {
  names: [],
  setNames: noop,
  collections: {},
  setCollections: noop,
  edit: null,
  setEdit: noop,
  selected: null,
  setSelected: noop,
  document: EMPTY_DOCUMENT,
  setDocument: noop,
}
