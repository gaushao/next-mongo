import { isEmpty } from 'lodash'
import Card from './Card'
import Create from './Create'

import Paper from '../paper'
import Button from '../button'
import useCollections from './context/hooks/useCollections'
import { useMemo } from 'react'

function CollectionList() {
  const { names, load } = useCollections()
  const hasCollections = !isEmpty(names)
  const collections = useMemo(
    () =>
      hasCollections
        ? names.map((name) => <Card collection={name} key={name} />)
        : null,
    [names]
  )
  return (
    <Paper margin='16px'>
      <Create />
      <Button onClick={load}>REFRESH</Button>
      <Paper
        hide={!hasCollections}
        margin='16px 0'
        padding='16px'
        border='1px solid gray'
      >
        {collections}
      </Paper>
    </Paper>
  )
}

export default CollectionList
