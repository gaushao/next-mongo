import { Document } from '../context/types'
import Button from '../../button'
import PreCodeJSON from '../../json/PreCodeJSON'
import useRemove from '../context/hooks/useRemove'
import useEdit from '../context/hooks/useEdit'

type Props = {
  document: Document
}
function Display({ document }: Props) {
  const { remove } = useRemove(document)
  const { setEdit } = useEdit()
  const edit = () => setEdit(document.id)
  return (
    <>
      <PreCodeJSON>{document}</PreCodeJSON>
      <Button onClick={remove}>REMOVE</Button>
      <Button onClick={edit}>EDIT</Button>
    </>
  )
}

export default Display
