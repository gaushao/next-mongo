import { Document } from '../context/types'
import Paper from '../../paper'
import useEdit from '../context/hooks/useEdit'
import Display from './Display'
import Edit from './Edit'

const CONTAINER_STYLE = {
  margin: '8px 0',
  padding: '8px',
  backgroundColor: 'lightgray',
  border: '1px solid gray',
}
type Props = {
  document: Document
}

function Card({ document }: Props) {
  const { edit } = useEdit()
  const isEditing = document.id === edit
  return (
    <Paper {...CONTAINER_STYLE}>
      {isEditing ? (
        <Edit document={document} />
      ) : (
        <Display document={document} />
      )}
    </Paper>
  )
}

export default Card
