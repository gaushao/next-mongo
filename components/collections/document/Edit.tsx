import { flow, isEmpty, omit } from 'lodash'
import Paper from '../../paper'
import Button from '../../button'
import TextInput from '../../input/Text'
import { Document } from '../context/types'
import useEdit from '../context/hooks/useEdit'
import useUpdate from '../context/hooks/useUpdate'
import { LOCKED_DOCUMENT_PROPERTIES } from '../context/constants'
import { useCallback, useEffect, useMemo, useState } from 'react'

type PropertyPairKey = string | null
type PropertyPairValue = string | null
type OnChangePropertyValue = (value: PropertyPairValue) => void
type AddProperty = (key: string) => void
type SetProperty = (key: string) => OnChangePropertyValue
type UnsetProperty = (key: string) => () => void

type PropertyProps = {
  name: string | null
  value: string | null
}
type CreatePropertyProps = {
  onCreate: AddProperty
}

function CreateProperty({ onCreate }: CreatePropertyProps) {
  const [name, setName] = useState<PropertyPairKey>(null)
  const add = () => {
    setName('')
  }
  const cancel = () => {
    setName(null)
  }
  const create = () => {
    onCreate(name)
    cancel()
  }
  const willCreate = name === null
  const isNamed = !isEmpty(name)
  return willCreate ? (
    <Button onClick={add}>ADD</Button>
  ) : (
    <Paper flex>
      <TextInput onChange={setName} value={name} />
      {isNamed && <Button onClick={create}>CREATE</Button>}
      <Button onClick={cancel}>X</Button>
    </Paper>
  )
}

type EditPropertyProps = PropertyProps & {
  onChange: OnChangePropertyValue
  onDelete: () => void
}

function EditProperty({ name, value, onChange, onDelete }: EditPropertyProps) {
  return (
    <Paper flex>
      <TextInput label={name} onChange={onChange} value={value || ''} />
      <Button onClick={onDelete}>X</Button>
    </Paper>
  )
}

type Props = {
  document: Document
}

function Edit({ document: remote }: Props) {
  const initial = useMemo(
    () => omit(remote, LOCKED_DOCUMENT_PROPERTIES),
    [remote]
  )
  const [document, setDocument] = useState(initial)
  useEffect(() => setDocument(initial), [initial])
  const { setEdit } = useEdit()
  const cancel = () => setEdit('')
  const { update, unset } = useUpdate(remote)
  const addProperty: AddProperty = useCallback(
    (key: string) => setDocument((curr) => ({ ...curr, [key]: '' })),
    [setDocument]
  )
  const setProperty: SetProperty = useCallback(
    (key: string) => (value: any) =>
      setDocument((curr) => ({ ...curr, [key]: value })),
    [setDocument]
  )
  const discardProperty: UnsetProperty = useCallback(
    (key: string) => () =>
      setDocument((curr) => {
        delete curr[key]
        return curr
      }),
    [setDocument]
  )
  const onDelete: UnsetProperty = useCallback(
    // unset form db if it's already remote or delete local with discardProperty
    (key: string) => (!!remote[key] ? unset(key) : discardProperty(key)),
    [remote]
  )
  const submit = useCallback(flow(update(document), cancel), [])
  return (
    <>
      <CreateProperty onCreate={addProperty} />
      {Object.keys(document).map((key) => (
        <EditProperty
          key={key}
          name={key}
          value={document[key]}
          onChange={setProperty(key)}
          onDelete={onDelete(key)}
        />
      ))}
      <Button onClick={cancel}>CANCEL</Button>
      <Button onClick={submit}>UPDATE</Button>
    </>
  )
}

export default Edit
