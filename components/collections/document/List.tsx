import { useEffect, useMemo } from 'react'
import { get } from 'lodash'
import useInsert from '../context/hooks/useInsert'
import useCollection from '../context/hooks/useCollection'
import Button from '../../button'
import Paper from '../../paper'
import Card from './Card'

type Props = {
  collection: string
}

function List({ collection }: Props) {
  const { result, reload } = useCollection(collection)
  const documents = get(result, 'documents', null)
  const { insert } = useInsert()
  const list = useMemo(
    () =>
      Array.isArray(documents)
        ? documents.map((document) => (
            <Card document={document} key={document.id} />
          ))
        : null,
    [documents, collection]
  )
  useEffect(() => {
    if (result && !documents) reload()
  }, [result, documents])
  return (
    <Paper margin='8px 0'>
      <Button onClick={insert}>INSERT</Button>
      {list}
    </Paper>
  )
}

export default List
