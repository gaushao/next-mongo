import styled from 'styled-components'
import Paper from '../paper'
import Button from '../button'
import { P } from '../text'
import DocumentList from './document/List'
import useSelection from './context/hooks/useSelection'
import useDrop from './context/hooks/useDrop'
import { useCallback } from 'react'

const Name = styled(P)`
  font-size: 18px;
  weight: 500;
  margin: 0 8px;
  ${({ isSelected }: { isSelected: boolean }) =>
    isSelected ? 'color: white;' : ''}
`
const NameContainer = styled(Paper)(
  ({ isSelected }: { isSelected: boolean }) => `
    ${isSelected ? '' : 'border: 1px solid gray;'}
    ${isSelected ? 'background-color: black;' : ''}
    padding: 8px;
  `
)

type Props = {
  collection: string
}

function CollectionCard({ collection }: Props) {
  const { selected, setSelected, unselect } = useSelection()
  const isSelected = selected === collection
  const select = useCallback(
    () => setSelected(collection),
    [setSelected, collection]
  )
  const { drop } = useDrop(collection)
  return (
    <Paper margin='8px 0'>
      <Paper flex justifyContent='space-between' width='100%'>
        <NameContainer isSelected={isSelected}>
          {isSelected ? (
            <Button onClick={unselect}>X</Button>
          ) : (
            <Button onClick={select}>SELECT</Button>
          )}
          <Name isSelected={isSelected}>{collection}</Name>
        </NameContainer>
        {isSelected && <Button onClick={drop}>DROP</Button>}
      </Paper>
      {isSelected && <DocumentList collection={collection} />}
    </Paper>
  )
}

export default CollectionCard
