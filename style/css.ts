import { kebabCase } from 'lodash'

const pairToCss = (key: string, value: string) =>
  value ? `${key}: ${value};` : ''

function css<T>(styles: T) {
  return Object.keys(styles).reduce(
    (acc, curr) => `
      ${acc}
      ${pairToCss(kebabCase(curr), styles[curr])}
    `,
    ''
  )
}

export default css
