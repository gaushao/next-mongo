import type { NextApiRequest, NextApiResponse } from 'next'
import { v4 as uuidv4 } from 'uuid'
import connect from '../../database/connect'

export type DocumentType = { id: string } & Record<string, string>

export default async (
  req: NextApiRequest,
  res: NextApiResponse<DocumentType>
) => {
  const id = uuidv4()
  const {
    body: { collection, document },
  } = req
  const { db } = await connect()
  await db.collection(collection).insertOne({
    ...document,
    id,
  })
  res.json(document)
}
