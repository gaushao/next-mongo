import { NextApiRequest, NextApiResponse } from 'next'
import connect from '../../database/connect'

export type RemoveResponse = NextApiResponse<string | Error>

async function remove(req: NextApiRequest, res: RemoveResponse) {
  try {
    const {
      body: { collection, document },
    } = req
    const { db } = await connect()
    await db.collection(collection).deleteOne({ id: document.id })
    res.json(collection)
  } catch (error) {
    res.json(error)
  }
}

export default remove
