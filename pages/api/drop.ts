import { NextApiRequest, NextApiResponse } from 'next'
import connect from '../../database/connect'

type DropResponse = {
  ok: boolean
  error: Error | null
}

async function drop<T = Record<string, string>>(
  req: NextApiRequest,
  res: NextApiResponse<DropResponse>
) {
  try {
    const {
      body: { collection },
    } = req
    const { db } = await connect()
    await db.collection<T>(collection).drop()
    res.json({ ok: true, error: null })
  } catch (error) {
    res.json({ ok: false, error })
  }
}

export default drop
