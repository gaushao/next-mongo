import type { NextApiRequest, NextApiResponse } from 'next'
import connect from '../../database/connect'
import { CollectionType } from '../../database/types'

async function collection(
  req: NextApiRequest,
  res: NextApiResponse<CollectionType[]>
) {
  const { db } = await connect()
  const {
    query: { name },
  } = req
  const result = collection
    ? await db.collection<CollectionType>(name.toString()).find().toArray()
    : null
  res.json(result)
}

export default collection
