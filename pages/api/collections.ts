import type { NextApiRequest, NextApiResponse } from 'next'
import connect from '../../database/connect'
import { CollectionType } from '../../database/types'

async function collections(
  req: NextApiRequest,
  res: NextApiResponse<CollectionType[]>
) {
  const { db } = await connect()
  res.json(await db.listCollections().toArray())
}

export default collections
