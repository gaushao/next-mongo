import { NextApiRequest, NextApiResponse } from 'next'
import { omit, pick } from 'lodash'
import connect from '../../database/connect'
import {
  DocumentType,
  UpdateDocumentQuery,
  HTTPMethod,
} from '../../database/types'
import { LOCKED_DOCUMENT_PROPERTIES } from '../../database/constants'

export interface UpdateRequest extends NextApiRequest {
  body: UpdateDocumentQuery
  method: HTTPMethod.PUT | HTTPMethod.DELETE
}

export type UpdateResponse = NextApiResponse<Partial<DocumentType>>

async function update(req: UpdateRequest, res: UpdateResponse) {
  try {
    const {
      body: { collection, document, options },
      method,
    } = req
    const { db } = await connect()
    switch (method) {
      case HTTPMethod.PUT:
        await db
          .collection(collection)
          .updateOne(
            pick(document, 'id'),
            { $set: omit(document, LOCKED_DOCUMENT_PROPERTIES) },
            options
          )
        res.status(200).json(document)
        break
      case HTTPMethod.DELETE:
        await db
          .collection(collection)
          .updateOne(
            pick(document, 'id'),
            { $unset: omit(document, LOCKED_DOCUMENT_PROPERTIES) },
            options
          )
        res.status(200).json(document)
        break
      default:
        res.setHeader('Allow', [HTTPMethod.PUT, HTTPMethod.DELETE])
        res.status(405).end(`Method ${method} Not Allowed`)
    }
  } catch (error) {
    res.json(error)
  }
}

export default update
