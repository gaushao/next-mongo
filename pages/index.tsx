import Paper from '../components/paper'
import { H1 } from '../components/text'
import Collections from '../components/collections'
import styled from 'styled-components'

const Title = styled(H1)`
  margin: 16px;
`

export default function Home() {
  return (
    <Paper>
      <Title>NextMongo</Title>
      <Collections />
    </Paper>
  )
}
